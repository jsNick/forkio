'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat');

sass.compiler = require('node-sass');

gulp.task('script', function () {
    return gulp.src('src/js/**/*.js')
    // .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('dest/js'));

});

gulp.task('style', function (done) {
    return gulp.src('src/s?ss/**/*.s?ss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(concat('app.css'))
        .pipe(gulp.dest('dest/css'));
    done();
});

gulp.task('script:vendor', function () {
    return gulp
        .src([
                'node_modules/jquery/dist/jquery.js',
                'node_modules/@fortawesome/fontawesome-free/js/all.js'
            ]
        )
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('dest/js'));


});

gulp.task('style:vendor', function () {
    return gulp
        .src(
            [
                'node_modules/normalize.css/normalize.css',
                'node_modules/@fortawesome/fontawesome-free/css/all.css'
            ]
        )
        .pipe(cleanCSS())
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('dest/css'));


});

gulp.task('fonts', done => {
    gulp
        .src('node_modules/@fortawesome/fontawesome-free/webfonts/*.*')
        .pipe(gulp.dest('dest/webfonts'))
    done();
});


gulp.task('img_svg', done => {
    gulp
        .src(
            [
                'node_modules/@fortawesome/fontawesome-free/svgs/**/*.*'
            ]
        )
        .pipe(gulp.dest('dest/svgs/'))
    done();
});

gulp.task('img', done => {
    gulp
        .src(
            [
                'src/img/*.*'
            ]
        )
        .pipe(gulp.dest('dest/img/'))
    done();
});

gulp.task('sprites', done => {
    gulp
        .src(
            [
                'node_modules/@fortawesome/fontawesome-free/sprites/*.*'
            ]
        )
        .pipe(gulp.dest('dest/sprites/'))
    done();
});



gulp.task('style:watch', function () {
    return gulp
        .watch(
            'src/**/*.s?ss',
            gulp.series('style')
        );
});

gulp.task('script:watch', function () {
    return gulp
        .watch(
            'src/js/**/*.js',
            gulp.series('script')
        );
});

gulp.task('forkio:watch', function () {
    return gulp
        .watch(
            'src/**/*.*',
            gulp.series(
                'script',
                'script:vendor',
                'style',
                'style:vendor',
                'fonts',
                'img',
                'sprites'
            )
        );
});

